package com.example.tmdbaz;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.example.tmdbaz.databinding.ActivityMovieInformationBinding;
import com.example.tmdbaz.model.Results;
import com.google.gson.Gson;

public class MovieInformationActivity extends AppCompatActivity {

    private ActivityMovieInformationBinding binding;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityMovieInformationBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        String jsonMovie = getIntent().getStringExtra("movie");

        Gson gson = new Gson();
        Results movie = gson.fromJson(jsonMovie, Results.class);

        binding.movieName.setText( movie.getOriginal_title() );
        binding.releaseDate.setText(movie.getRelease_date());
        binding.language.setText( movie.getOriginal_language() );
        binding.overview.setText( movie.getOverview() );
        binding.voteAverage.setText( String.valueOf(movie.getVote_average()));

        Glide.with(this)
                .load( "https://image.tmdb.org/t/p/original/" + movie.getPoster_path() )
                .centerCrop()
                .placeholder(R.drawable.ic_launcher_foreground)
                .into(binding.imageMovie);
    }
}