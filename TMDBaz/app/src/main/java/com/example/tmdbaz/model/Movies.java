package com.example.tmdbaz.model;

import java.util.List;

public class Movies {

    private Integer page;
    private List<Results> results;

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public List<Results> getResults() {
        return results;
    }

    public void setResults(List<Results> results) {
        this.results = results;
    }
}
