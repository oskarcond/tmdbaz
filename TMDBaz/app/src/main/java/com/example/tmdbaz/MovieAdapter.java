package com.example.tmdbaz;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.tmdbaz.databinding.RowMoviesBinding;
import com.example.tmdbaz.model.Movies;
import com.example.tmdbaz.model.Results;
import com.google.gson.Gson;

import java.util.List;

public class MovieAdapter extends RecyclerView.Adapter <RecyclerView.ViewHolder>{

    private List<Results> infoMovies;
    Context context;

    LayoutInflater inflater;

    public MovieAdapter(Context context, List<Results> infoMovies){
        this.infoMovies = infoMovies;
        this.context = context;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        RowMoviesBinding itemBinding = RowMoviesBinding.inflate(layoutInflater,parent,false);
        return new MoviesViewHolder (itemBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        MoviesViewHolder holder1 = (MoviesViewHolder) holder;
        holder1.binding.movieName.setText(infoMovies.get(position).getOriginal_title());
        holder1.binding.movieReleaseDate.setText(infoMovies.get(position).getRelease_date());

        Glide.with(context)
                .load("https://image.tmdb.org/t/p/w500"+infoMovies.get(position).getPoster_path() )
                .centerCrop()
                .placeholder(R.drawable.ic_launcher_foreground)
                .into( holder1.binding.imageMovie );
    }

    @Override
    public int getItemCount() { return infoMovies.size(); }

    public class MoviesViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private final RowMoviesBinding binding;

        public MoviesViewHolder (RowMoviesBinding binding){
            super(binding.getRoot());
            this.binding = binding;

            binding.buttonDetails.setOnClickListener(view -> {
                Gson gson = new Gson();
                String jsonMoview = gson.toJson( infoMovies.get( getAdapterPosition() ) );

                Intent intent = new Intent (context, MovieInformationActivity.class);
                intent.putExtra("movie", jsonMoview);
                context.startActivity(intent);

            });
        }

        @Override
        public void onClick(View view) {

        }
    }
}
