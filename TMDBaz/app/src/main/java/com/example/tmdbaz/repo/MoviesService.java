package com.example.tmdbaz.repo;

import com.example.tmdbaz.model.Movies;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface MoviesService {

    //CRUD create, read, update, delete servicios Restful

    String APIKEY = "7216142ffd863b71de8862a44965b9dc";

    @GET("/3/discover/movie?sort_by=popularity.desc")
    Call<Movies> readMostPopularMovies(@Query("api_key") String api_key);

    @GET("/3/discover/movie?now_playing")
    Call<Movies> readNowPlayingMovies(@Query("api_key") String api_key);

}
