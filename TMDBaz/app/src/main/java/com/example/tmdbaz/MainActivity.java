package com.example.tmdbaz;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.example.tmdbaz.databinding.ActivityMainBinding;
import com.example.tmdbaz.model.Movies;
import com.example.tmdbaz.repo.MoviesService;
import com.example.tmdbaz.repo.Service;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "mainActivity";
    private ActivityMainBinding binding;
    MovieAdapter adapter;
    private List<Movies> lista = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());


        Service.createMovieService().readNowPlayingMovies(MoviesService.APIKEY).enqueue(new Callback<Movies>() {
            @Override
            public void onResponse(Call<Movies> call, Response<Movies> response) {
                Log.d(TAG, "Busqueda exitosa");

                binding.rv1.setLayoutManager(new LinearLayoutManager(MainActivity.this, LinearLayoutManager.HORIZONTAL, false));
                MovieAdapter adapter = new MovieAdapter( MainActivity.this, response.body().getResults());
                binding.rv1.setAdapter(adapter);

            }

            @Override
            public void onFailure(Call<Movies> call, Throwable t) {
                Log.d(TAG, "Error al agregar");

            }
        });



        Service.createMovieService().readMostPopularMovies(MoviesService.APIKEY).enqueue(new Callback<Movies>() {
            @Override
            public void onResponse(Call<Movies> call, Response<Movies> response) {
                Log.d(TAG, "Busqueda exitosa");

                binding.rv2.setLayoutManager(new LinearLayoutManager(MainActivity.this, LinearLayoutManager.HORIZONTAL, false));
                MovieAdapter adapter = new MovieAdapter( MainActivity.this, response.body().getResults());
                binding.rv2.setAdapter(adapter);

            }

            @Override
            public void onFailure(Call<Movies> call, Throwable t) {
                Log.d(TAG, "Error al agregar");

            }
        });
    }

}